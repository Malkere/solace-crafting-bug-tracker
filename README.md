# README #

### What is this repository for? ###

* This repository was created specifically to allow public bug reporting in a trackable transparent manner.

### How do I post a bug? ###

* You can login with Google, Microsoft, and Apple accounts, create a new bitbucket specific account, or post issues anonymously without logging in.
* Head to the issues tab and search to see if the bug or suggestion you want to report/make is already present. In which case please consider adding a comment or additional information instead of creating a new issue.
